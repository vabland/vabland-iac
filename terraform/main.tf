provider "google" {
  credentials = "${file("./key.json")}"
  project = var.project_id
  region = var.region
  zone = var.zone
}

terraform {
  backend "gcs" {
  }
}

module "api-cluster" {
  source = "./modules/api-cluster"
  region = "${var.region}"
}

module "cloud-build" {
  source = "./modules/cloud-build"
  project_number = "${var.project_number}"
}