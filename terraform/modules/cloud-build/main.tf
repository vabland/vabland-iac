resource "google_project_iam_binding" "cluster-admin" {
  role = "roles/container.clusterAdmin"

  members = [
    "serviceAccount:${var.project_number}@cloudbuild.gserviceaccount.com",
  ]
}

resource "google_project_iam_binding" "deployment" {
  role = "roles/deploymentmanager.editor"

  members = [
    "serviceAccount:${var.project_number}@cloudbuild.gserviceaccount.com",
  ]
}

resource "google_project_iam_binding" "container" {
  role = "roles/container.developer"

  members = [
    "serviceAccount:${var.project_number}@cloudbuild.gserviceaccount.com",
  ]
}