variable "region" {
  type = string
  description = "Region of Cluster and Pool nodes"
}