variable "project_id" {
  type = string
  description = "The Google Cloud Project Id"
}

variable "project_number" {
  type = string
  description = "The Google Cloud Project Number"
}

variable "region" {
  type = string
  description = "Region of Google Cloud Project"
}

variable "zone" {
  type = string
  description = "Zone within the region of Google Cloud Project"
}