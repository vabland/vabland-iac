# Vabland IaC (Infrastructure as Code)

Responsible to generate GCP project using terraform

-----

### Requirements

- gcloud
- terraform

-----

### Commands

Execute the following command to create a empty new project with the basic infrastructure:
```bash
./bootstrap.sh
```

-----

### Terraform env variables required

```
export TF_VAR_project_id=<project_id>
export TF_VAR_project_number=<project_number>
export TF_VAR_region=<region>
export TF_VAR_zone=<zone>
``  