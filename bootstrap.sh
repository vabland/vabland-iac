#!/usr/bin/env bash

# Colors
BLUE='\033[0;34m'
RED='\033[0;31m'
L_GREEN='\033[1;32m'
YELLOW='\033[1;33m'
NC='\033[0m'

STEPS=10

rm -rf .terraform/

echo -e "\n${L_GREEN} > ${RED}0/${STEPS} ${L_GREEN} - SETUP ${NC}"

# Asking for names
GCLOUD_ACTIVE_ACCOUNT=$(gcloud auth list --filter=status:ACTIVE --format="value(account)")
read -p "$(echo -e ${L_GREEN} \> ${NC}Use this \(${YELLOW}${GCLOUD_ACTIVE_ACCOUNT}${NC}\) GCP Account? ${YELLOW}\[YES\(default\)\|NO\]${NC}: )" ACCOUNT_OK

if [[ "$ACCOUNT_OK" == "NO" || "$ACCOUNT_OK" == "no" || "$ACCOUNT_OK" == "N" || "$ACCOUNT_OK" == "n" ]]; then
    echo -e "\n${L_GREEN} > ${RED}1/${STEPS} ${L_GREEN} - GCP LOGIN ${NC}"
    gcloud auth login --quiet
else
    echo -e "\n${L_GREEN} > ${RED}1/${STEPS} ${L_GREEN} - GCP LOGIN: ${YELLOW}${GCLOUD_ACTIVE_ACCOUNT} ${NC}"
    echo -e "\nUsing: ${YELLOW}${GCLOUD_ACTIVE_ACCOUNT}${NC}\n"
fi

RANDOM_NUMBER=$(( ( RANDOM % 100000 )  + 1 ))
PROJECT_ID_DEFAULT="vabland-game-"$RANDOM_NUMBER
read -p "$(echo -e ${L_GREEN} \> ${NC}Enter the PROJECT ID \(lowercase letters, digits or hyphens\) ${YELLOW}\[$PROJECT_ID_DEFAULT\]${NC}: )" PROJECT_ID
PROJECT_ID="${PROJECT_ID:-$PROJECT_ID_DEFAULT}"

PROJECT_NAME_DEFAULT="vabland"
read -p "$(echo -e ${L_GREEN} \> ${NC}Enter the PROJECT NAME ${YELLOW}\[$PROJECT_NAME_DEFAULT\]${NC}: )" PROJECT_NAME
PROJECT_NAME="${PROJECT_NAME:-$PROJECT_NAME_DEFAULT}"

REGION_DEFAULT="southamerica-east1"
read -p "$(echo -e ${L_GREEN} \> ${NC}Enter the REGION ${YELLOW}\[$REGION_DEFAULT\]${NC}: )" REGION
REGION="${REGION:-$REGION_DEFAULT}"

ZONE_DEFAULT="southamerica-east1-a"
read -p "$(echo -e ${L_GREEN} \> ${NC}Enter the ZONE ${YELLOW}\[$ZONE_DEFAULT\]${NC}: )" ZONE
ZONE="${ZONE:-$ZONE_DEFAULT}"

# Creates Project
echo -e "\n${L_GREEN} > ${RED}2/${STEPS} ${L_GREEN} - GCP PROJECT CREATION ${NC}"
gcloud projects create $PROJECT_ID --name="$PROJECT_NAME"

echo -e "\n${L_GREEN} > ${RED}3/${STEPS} ${L_GREEN} - GCP PROJECT SETUP DEFAULTS ${NC}"
gcloud config set project $PROJECT_ID
PROJECT_NUMBER=$(gcloud projects list --filter="$PROJECT_ID" --format="value(PROJECT_NUMBER)")

echo -e "\n${L_GREEN} > ${RED}4/${STEPS} ${L_GREEN} - GCP BILLING ACCOUNT ${NC}"
BILLING_ACCOUNT_ID=$(gcloud beta billing accounts list --filter="open=true" --format="value(name)" --quiet)
gcloud beta billing projects link $PROJECT_ID --billing-account=$BILLING_ACCOUNT_ID

echo -e "\n${L_GREEN} > ${RED}5/${STEPS} ${L_GREEN} - GCP ENABLING APIs ${NC}"
gcloud services enable pubsub.googleapis.com
gcloud services enable datastore.googleapis.com
gcloud services enable cloudresourcemanager.googleapis.com
gcloud services enable container.googleapis.com
gcloud services enable cloudbuild.googleapis.com
gcloud services enable containerregistry.googleapis.com
gcloud services enable sourcerepo.googleapis.com

echo -e "\n${L_GREEN} > ${RED}6/${STEPS} ${L_GREEN} - GCP ACCOUNT CREATION FOR TERRAFORM ${NC}"
gcloud iam service-accounts create iac-account --display-name "IaC Account"
gcloud iam service-accounts keys create ./key.json --iam-account iac-account@$PROJECT_ID.iam.gserviceaccount.com
gcloud projects add-iam-policy-binding $PROJECT_ID \
  --member serviceAccount:iac-account@$PROJECT_ID.iam.gserviceaccount.com \
  --role roles/owner

echo -e "\n${L_GREEN} > ${RED}7/${STEPS} ${L_GREEN} - GCP BUCKET CREATION FOR TERRAFORM STATE ${NC}"
gsutil mb -c regional -l $REGION -p $PROJECT_ID gs://$PROJECT_ID-tf-state
gsutil acl ch -u iac-account@$PROJECT_ID.iam.gserviceaccount.com:O gs://$PROJECT_ID-tf-state


echo -e "\n${L_GREEN} > ${RED}8/${STEPS} ${L_GREEN} - TERRAFORM INIT ${NC}"
terraform init \
-backend-config "bucket=${PROJECT_ID}-tf-state" \
-backend-config "credentials=key.json" \
terraform/

echo -e "\n${L_GREEN} > ${RED}9/${STEPS} ${L_GREEN} - TERRAFORM APPLY ${NC}"
terraform apply -auto-approve -input=false -var "project_id=${PROJECT_ID}" -var "project_number=${PROJECT_NUMBER}" -var "region=${REGION}" -var "zone=${ZONE}" terraform/

echo -e "\n${L_GREEN} > ${RED}10/${STEPS} ${L_GREEN} - END ${NC}"



echo -e "\n\n${L_GREEN}--------------------------------------------------------------------------------------------${NC}"
echo -e "${L_GREEN}|                                                                                           |${NC}"
echo -e "${L_GREEN}|                                                                                           |${NC}"
echo -e "${L_GREEN}|${NC}                   Executing ${YELLOW}terraform${NC} with the following variables:                       ${L_GREEN}|${NC}"
echo -e "${L_GREEN}|                                                                                           |${NC}"


SPACE_LENGTH_ID=$((43 - ${#PROJECT_ID}))
echo -ne "${L_GREEN}|${NC}                      ${BLUE}export${YELLOW} TF_VAR_project_id${NC}=${L_GREEN}${PROJECT_ID}${NC}"
for i in $( seq 0 $SPACE_LENGTH_ID )
do
   echo -ne " "
done
echo -e "${L_GREEN}|${NC}"


SPACE_LENGTH_NUMBER=$((39 - ${#PROJECT_NUMBER}))
echo -ne "${L_GREEN}|${NC}                      ${BLUE}export${YELLOW} TF_VAR_project_number${NC}=${L_GREEN}${PROJECT_NUMBER}${NC}"
for i in $( seq 0 $SPACE_LENGTH_NUMBER )
do
   echo -ne " "
done
echo -e "${L_GREEN}|${NC}"


SPACE_LENGTH_REGION=$((47 - ${#REGION}))
echo -ne "${L_GREEN}|${NC}                      ${BLUE}export${YELLOW} TF_VAR_region${NC}=${L_GREEN}${REGION}${NC}"
for i in $( seq 0 $SPACE_LENGTH_REGION )
do
   echo -ne " "
done
echo -e "${L_GREEN}|${NC}"


SPACE_LENGTH_ZONE=$((49 - ${#ZONE}))
echo -ne "${L_GREEN}|${NC}                      ${BLUE}export${YELLOW} TF_VAR_zone${NC}=${L_GREEN}${ZONE}${NC}"
for i in $( seq 0 $SPACE_LENGTH_ZONE )
do
   echo -ne " "
done
echo -e "${L_GREEN}|${NC}"


echo -e "${L_GREEN}|                                                                                           |${NC}"
echo -e "${L_GREEN}|                                                                                           |${NC}"
echo -e "${L_GREEN}--------------------------------------------------------------------------------------------${NC}\n\n"
